export interface Export {
  idExport: number;
  nameExport: string;
  startDate: Date;
  user: string;
  lokal: string;
}
