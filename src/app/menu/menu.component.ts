import { Component, OnInit } from '@angular/core';
import { Lokal } from 'src/models/lokal';
import { HttpService } from '../services/http.service';
import { first } from 'rxjs/operators';
import { Export } from 'src/models/export';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  startDateFilter: Date;
  finishDateFilter: Date;
  minFinishDate: Date;
  lokal: Lokal[];
  selectedLokalFilter: Lokal;
  export: Export[];
  constructor(private http: HttpService) { }

  ngOnInit() {
this.http.getLokals().pipe(first())
.subscribe(
  res => {
    this.lokal = res;  }
);
  }

  getMinFinishDate() {
    if (this.startDateFilter) {
      this.minFinishDate = this.startDateFilter;
      this.minFinishDate.setHours(this.minFinishDate.getHours() + 1);
  }
}

clearFinish() {
  if (this.finishDateFilter) {
    this.finishDateFilter = null;
  }
  }

getExport() {
  const start = this.startDateFilter.getTime();
  const finish = this.finishDateFilter.getTime();
  this.http.getExports(this.selectedLokalFilter.name, this.startDateFilter, this.finishDateFilter);
}


}
