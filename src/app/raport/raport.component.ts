import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-raport',
  templateUrl: './raport.component.html',
  styleUrls: ['./raport.component.css']
})
export class RaportComponent implements OnInit, OnDestroy {

  constructor(private http: HttpService) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.http.export2.next([]);
  }
}
