import { Component } from '@angular/core';
import {DialogService} from 'primeng/api';
import { RaportComponent } from './raport/raport.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DialogService]
})
export class AppComponent {

  constructor(public dialogService: DialogService) {}

  show() {
    const ref = this.dialogService.open(RaportComponent, {
        header: 'Raport',
        width: '70%',
        height: '50vh'
    });
}
}
