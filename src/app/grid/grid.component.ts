import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../services/http.service';
import { first } from 'rxjs/operators';
import { Export } from 'src/models/export';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit, OnDestroy {
exportsList: Array<Export> = [];
subManager = new Subscription();
cols: any[];
  constructor(private http: HttpService) { }

  ngOnInit() {

    this.cols = [
      { field: 'nameExport', header: 'Nazwa' },
      {field: 'startDate', header: 'Data' },
      { field: 'startDate', header: 'Godzina' },
      { field: 'user', header: 'Użytkownik' },
      { field: 'lokal', header: 'Lokal' }
  ];

    const sub1 = this.http.export2
    .subscribe(
      res => {
        this.exportsList = res;
      }
    );
    this.subManager.add(sub1);
  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
     }
   }

}
