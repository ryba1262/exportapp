import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../services/http.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit, OnDestroy {
  switchLoader = false;
  subManager = new Subscription();
  constructor(private http: HttpService) {
   const sub1 = this.http.loadingStatus.subscribe(
      (state) => {
          this.switchLoader = state;
        }
    );
   }

  ngOnInit() {

  }

  ngOnDestroy() {
    if (this.subManager) {
      this.subManager.unsubscribe();
     }
   }

}
