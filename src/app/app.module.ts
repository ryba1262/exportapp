import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {DropdownModule} from 'primeng/dropdown';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { GridComponent } from './grid/grid.component';
import { FormsModule } from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
import { HttpClientModule } from '@angular/common/http';
import {TableModule} from 'primeng/table';
import { RaportComponent } from './raport/raport.component';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {ToastModule} from 'primeng/toast';
import { HttpService } from './services/http.service';
import { MessageService } from 'primeng/api';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    GridComponent,
    RaportComponent,
    LoaderComponent
  ],
  entryComponents: [
    RaportComponent
],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    DropdownModule,
    CalendarModule,
    TableModule,
    DynamicDialogModule,
    ToastModule,
  ],
  providers: [
    HttpService, MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
