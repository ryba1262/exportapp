import { Injectable } from '@angular/core';
import { HttpParams} from '@angular/common/http';
import { HttpClient} from '@angular/common/http';
import { Lokal } from 'src/models/lokal';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { Export } from 'src/models/export';
import { first } from 'rxjs/operators';
import { MessageService } from 'primeng/api';
import { urlApi } from '../enums/urlApi';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  export2 = new BehaviorSubject<Array<Export>>([]);
  private _loading = false;
  loadingStatus = new Subject<boolean>();
  readonly param = new HttpParams().set('apiKey', urlApi.param);
  constructor(private http: HttpClient,
              private messageService: MessageService
    ) { }

  getLokals(): Observable<Array<Lokal>> {
    return this.http.get<Array<Lokal>>(urlApi.URL_DBLOKAL, {params: this.param});
   }

   getExports(selectedLokalFilter, startDateFilter, finishDateFilter) {
    return this.http.get<Array<Export>>(urlApi.URL_DBEXPORTS, {params: this.param}).pipe(first())
    .subscribe(
      res => {
        this.loadingStatus.next(true);
        let export1: Export[];
        export1 = res
        .filter( exp => exp.lokal === selectedLokalFilter)
        .filter( exp2 => new Date(exp2.startDate) >= new Date(startDateFilter))
        .filter( exp2 => new Date(exp2.startDate) <= new Date(finishDateFilter));
        this.export2.next(export1);
        this.loadingStatus.next(false);
        if (export1.length > 0) {
          this.messageService.add({severity:'success', summary:'Sukces!', detail:'Pobrano listę exportów'});
        } else {
          this.messageService.add({severity:'warn', summary: 'Niepowodzenie!', detail:'Nie znaleziono żadnych pozycji'});
        }

      }
    );
   }
}



